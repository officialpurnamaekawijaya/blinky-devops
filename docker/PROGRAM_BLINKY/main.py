import RPi.GPIO as GPIO
import time
import urllib.request

GPIO.setwarnings(False)

GPIO.setmode(GPIO.BOARD)
pin_ldr = 16
GPIO.setup(18, GPIO.OUT)
GPIO.output(18, GPIO.LOW)
GPIO.setup(11, GPIO.IN)

def rc_time_ldr (pin_ldr):
    intensitas = 0 
    GPIO.setup(pin_ldr, GPIO.OUT)
    GPIO.output(pin_ldr, GPIO.LOW)
    time.sleep(0.1)
    GPIO.setup(pin_ldr, GPIO.IN)
    while (GPIO.input(pin_ldr) == GPIO.LOW):
        intensitas += 1
    return intensitas
#================================================================
print ("")
print("program untuk menghitung nyala LED oleh LDR")
while True:
    hitungan = 0
    waktu_tidur = 0.5
    led_sudah_nyala = 0
    led_suruh_nyala = 3 # Diubah untuk memanipulasi nyala led
    tombol = GPIO.input(11)
    if tombol == False:
        print ("")
        print ('tombol ditekan')
        while led_sudah_nyala < led_suruh_nyala :
            led_sudah_nyala = led_sudah_nyala + 1
            GPIO.output(18, GPIO.HIGH)
            analogLDR = rc_time_ldr (pin_ldr)
            if analogLDR <= 1500 :
                time.sleep (waktu_tidur)
                hitungan = hitungan + 1
                print ('hitungan bertambah menjadi =', hitungan, "hitungan sekarang", hitungan)
                GPIO.output(18, GPIO.LOW)
                time.sleep (waktu_tidur)
            if analogLDR > 2000:
                time.sleep (waktu_tidur)
                hitungan = ((hitungan + 1) - 1)
                print ('hitungan tidak bertambah', "      hitungan sekarang", hitungan)
                GPIO.output(18, GPIO.LOW)
                time.sleep (waktu_tidur)
            else:
                pass
            
        with urllib.request.urlopen('https://www.monitoring.firmandev.tech/index.php/data?set=YMukbp35unN7Emmoj0SfI4RhRoiTGlMJ&log=LDR%20mendeteksi%20nyala%20led%20' + str(hitungan) + '%20kali') as response:
            if response.getcode() == 200:
                print('Data Terkirim')
            else:
                pass
            
    if tombol == True:
        GPIO.output(18, GPIO.LOW)
        time.sleep (0.5)
    else:
        pass
    GPIO.cleanup
    #-------> resstful nilai "hitungan" dan nilai "led_suruh_nyala"
